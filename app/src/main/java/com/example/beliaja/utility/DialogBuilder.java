package com.example.beliaja.utility;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.beliaja.R;
import com.example.beliaja.view.activity.LoginActivity;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.FadingCircle;

public class DialogBuilder {

    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private ProgressDialog prgdialog;

    public void showDialogLoading(final Context context){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_loading, null);

        builder = new AlertDialog.Builder(context);

        builder.setView(view);

        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.dialog_progressbar);
        Sprite fadingCircle = new FadingCircle();
        progressBar.setIndeterminateDrawable(fadingCircle);

        dialog = builder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.show();
    }

    public void showDialogRegSuccess(final Context context){
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.dialog_reg_success, null);

        builder = new AlertDialog.Builder(context);

        builder.setView(view);

        Button btnLogin = (Button) view.findViewById(R.id.dialog_reg_btnSukses);

        dialog = builder.create();
        dialog.setCancelable(true);
        dialog.show();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
    }

    public void showDialogPrg(final Context context){
        prgdialog = new ProgressDialog(context);
        prgdialog.setMessage("Loading...");
        prgdialog.show();
    }

    public void dismissDialog(){
        dialog.dismiss();
    }

    public void dismissPrg(){ prgdialog.dismiss(); }
}
