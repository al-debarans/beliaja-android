package com.example.beliaja.utility;

import android.widget.ImageView;

import com.example.beliaja.R;
import com.squareup.picasso.Picasso;

public class ImgDownloader {

    public static void downloadAndSetImg(String url, ImageView imageView_id){
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.no_preview)
                .error(R.drawable.no_preview)
                .into(imageView_id);
    }
}
