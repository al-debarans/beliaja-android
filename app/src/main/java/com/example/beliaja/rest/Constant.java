package com.example.beliaja.rest;

public class Constant {
    public static final String BASE_URL = "http://192.168.43.110/beliaja/api/";
    public static final String PROF_URL = BASE_URL.replace("api/", "assets/pict/profile/");
    public static final String PROD_URL = BASE_URL.replace("api/", "assets/pict/product/");
    public static final String STORE_URL = BASE_URL.replace("api/", "assets/pict/store/");

}
