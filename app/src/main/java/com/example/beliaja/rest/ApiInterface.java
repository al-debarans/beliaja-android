package com.example.beliaja.rest;

import com.example.beliaja.model.CallResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    // region Login
    @FormUrlEncoded
    @POST("pembeli/login")
    Call<CallResponse> postLogin(
            @Field("pembeli_email") String pembeli_email,
            @Field("pembeli_password") String pembeli_password
    );
    // endregion

    // region Register
    @FormUrlEncoded
    @POST("pembeli/register")
    Call<CallResponse> postRegister(
            @Field("pembeli_nama") String pembeli_nama,
            @Field("pembeli_email") String pembeli_email,
            @Field("pembeli_password") String pembeli_password,
            @Field("pembeli_hp") String pembeli_hp,
            @Field("pembeli_alamat") String pembeli_alamat
    );
    // endregion

    // region Get Pembeli data
    @GET("pembeli/profile")
    Call<CallResponse> getProfile(
            @Query("pembeli_email") String profile
    );
    // endregion

    // region Get Produk
    @GET("produk")
    Call<CallResponse> getProduk(
            @Query("pembeli_id") String pembeli_id,
            @Query("produk_id") String produk_id,
            @Query("toko_id") String toko_id,
            @Query("produk_nama") String produk_nama
    );
    // endregion

    // region Get Wishlist
    @GET("wishlist")
    Call<CallResponse> getWishlist(
            @Query("pembeli_id") String pembeli_id
    );
    // endregion

    // region Add Wishlist
    @FormUrlEncoded
    @POST("wishlist/wishlist")
    Call<CallResponse> postWishlist(
            @Field("pembeli_id") String pembeli_id,
            @Field("produk_id") String produk_id
    );
    // endregion

    // region Dell Wishlist
    @FormUrlEncoded
    @POST("wishlist/unwishlist")
    Call<CallResponse> postUnwishlist(
            @Field("pembeli_id") String pembeli_id,
            @Field("produk_id") String produk_id
    );
    // endregion

    // region Add Cart
    @FormUrlEncoded
    @POST("keranjang/tambah")
    Call<CallResponse> postKeranjangAdd(
            @Field("pembeli_id") String pembeli_id,
            @Field("produk_id") String produk_id,
            @Field("jumlah") String jumlah,
            @Field("notes") String notes
    );
    // endregion

    // region Dell Cart
    @FormUrlEncoded
    @POST("keranjang/hapus")
    Call<CallResponse> postKeranjangDell(
            @Field("pembeli_id") String pembeli_id,
            @Field("produk_id") String produk_id
    );
    // endregion

    // region Get Cart
    @GET("keranjang")
    Call<CallResponse> getKeranjang(
            @Query("pembeli_id") String pembeli_id,
            @Query("status_prod") String status
    );
    // endregion

    // region Order Add
    @FormUrlEncoded
    @POST("order/add")
    Call<CallResponse> postOrder(
            @Field("pembeli_id") String pembeli_id
    );
    // endregion

    // region Order Get
    @GET("order")
    Call<CallResponse> getOrder(
            @Query("pembeli_id") String pembeli_id
    );
    // endregion

    // region Order Get
    @GET("order/history")
    Call<CallResponse> getHistory(
            @Query("pembeli_id") String pembeli_id
    );
    // endregion

    // region Order list
    @FormUrlEncoded
    @POST("order/list")
    Call<CallResponse> postOrderList(
            @Field("order_id") String order_id
    );
    // endregion

    // region Order Bayar
    @FormUrlEncoded
    @POST("order/pay")
    Call<CallResponse> postBayar(
            @Field("pembeli_id") String pembeli_id,
            @Field("order_id") String order_id
    );
    // endregion

    // region Order Get
    @GET("toko/profile")
    Call<CallResponse> getToko(
            @Query("toko_id") String toko_id
    );
    // endregion

}
