package com.example.beliaja.presenter.Profile;

import android.util.Log;
import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Pembeli;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.ImgDownloader;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter implements ProfileView.Presenter {

    private final ProfileView.View view;

    public ProfilePresenter(ProfileView.View view) {
        this.view = view;
    }

    @Override
    public void fetchProfile(String email) {
        profile(email);
    }

    private void profile(String email){
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);

        Call<CallResponse> call = apiInterface.getProfile(email);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Pembeli> profileDetail = response.body().getProfile();
                    view.displayProfile(profileDetail);
                    view.dismissProgressBar();
                }
                else{
                    view.dismissProgressBar();
                    switch (response.code()) {
                        case 404:
                            view.displayToastError(404, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
    }
}
