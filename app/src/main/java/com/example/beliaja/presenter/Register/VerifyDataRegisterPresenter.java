package com.example.beliaja.presenter.Register;

import android.text.TextUtils;

import com.example.beliaja.base.Presenter;

public class VerifyDataRegisterPresenter implements Presenter<RegisterContract> {

    private RegisterContract mContract;

    @Override
    public void onAttach(RegisterContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

    private boolean isAllDataFilled(String nama, String email, String password, String c_password, String phone) {

        return !TextUtils.isEmpty(nama) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) &&
                !TextUtils.isEmpty(c_password) && !TextUtils.isEmpty(phone);
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password){
        return password.length() < 5;
    }

    public void submitRegister(String nama, String email, String password, String phone, String alamat) {
        if(isAllDataFilled(nama, email, password, alamat, phone)) {
            if(!isEmailValid(email)) {
                mContract.onShowToast("Wrong email format");
            }
            else if (isPasswordValid(password)) {
                mContract.onShowToast("Password char at least 5");
            }
            else {
                mContract.onPostRegister(nama, email, password, phone, alamat);
            }
        } else {
            mContract.onShowToast("All data must be filled");
        }
    }
}
