package com.example.beliaja.presenter.Cart;

import android.util.Log;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartAdapterPresenter implements CartView.AdapterPresenter {

    private final CartView.AdapterView adapterView;

    public CartAdapterPresenter(CartView.AdapterView adapterView) {
        this.adapterView = adapterView;
    }

    @Override
    public void sendRemoveCart(String user_id, String prod_id, int position, int cartObjSize) {
        removeCart(user_id, prod_id, position, cartObjSize);
    }

    @Override
    public void textUbahEvent(String produk_id, String nama, String harga, String url_img) {
        adapterView.textUbahHandler(produk_id, nama, harga, url_img);
    }

    @Override
    public void textTitleEvent(String toko_id, String produk_id) {
        adapterView.textTitleHandler(toko_id, produk_id);
    }

    @Override
    public void onIncreaseEvent(String harga) {
        adapterView.increaseHandler(harga);
    }

    @Override
    public void onDecreaseEvent(String harga) {
        adapterView.decreaseHandler(harga);
    }

    private void removeCart(String u_id, String prod_id, final int position, final int sizeOfObj){

        adapterView.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postKeranjangDell(u_id, prod_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    adapterView.removeSuccess(position, sizeOfObj);
                    adapterView.refreshAdapter(true);
                }
                else{
                    switch (response.code()) {
                        case 501:
                            adapterView.dismissProgressBar();
                            adapterView.displayToastError(501, "");
                            break;
                        default:
                            adapterView.dismissProgressBar();
                            adapterView.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                adapterView.displayToastError(0, t.toString());
            }
        });
    }
}
