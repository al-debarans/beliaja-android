package com.example.beliaja.presenter.Wishlist;

import com.example.beliaja.model.Wishlist;

import java.util.List;

public interface WishlistView {

    interface View {
        void displayToast(int code, String onFailure);
        void initAdapter(List<Wishlist> wishlistsList);
        void isEmpty(boolean status);
        void swipeRefreshEvent(boolean status);
    }

    interface Presenter {
        void fetchWishlist(String user_id);
    }

    interface AdapterVIew {
        void removeSuccess(int position, int sizeofObj);
        void displayToastError(int code, String onFailure);
        void displayProgressBar();
        void btnBuyHandler(String prod_id, String prod_nama, String harga, String url);
        void cardHandler(String toko_id, String prod_id);
    }

    interface AdapterPresenter {
        void sendWishlistStatus(String user_id, String produk_id, int position, int sizeOfObj);
        void btnBuyClickEvent(String prod_id, String prod_nama, String harga, String url);
        void cardClickEvent(String toko_id, String prod_id);
    }
}
