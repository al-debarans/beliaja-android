package com.example.beliaja.presenter.Order;

import android.view.View;
import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Order;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderPresenter implements OrderView.Presenter {

    private final OrderView.View view;

    public OrderPresenter(OrderView.View view) {
        this.view = view;
    }

    @Override
    public void fetchOrder(String user_id) {
        order(user_id);
    }

    @Override
    public void onRefreshAdapterEvent(boolean status) {
        view.refreshAdapter(status);
    }

    private void order(String user_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);

        Call<CallResponse> call = apiInterface.getOrder(user_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Order> orderDetail = response.body().getOrder();
                    view.initAdapter(orderDetail);

                    if(orderDetail.size() > 0){
                        view.isEmpty(false);
                    }
                    else{
                        view.isEmpty(true);
                    }
                }
                else{
                    switch (response.code()) {
                        case 400:
                            view.displayToastError(400, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
        view.swipeRefreshEvent(false);
    }
}
