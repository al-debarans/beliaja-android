package com.example.beliaja.presenter.Home;

import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.example.beliaja.R;
import com.example.beliaja.base.Presenter;

public class HomePresenter implements Presenter<HomeContract> {

    private HomeContract mContract;

    @Override
    public void onAttach(HomeContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

//    private boolean fragmentSelected(Fragment fragment, MenuItem item){
//
//        switch (item.getItemId()) {
//            case R.id.navigation_home:
//
//                mContract.showToolbarSearch();
//                mContract.showToolbarWishlist();
//                mContract.hideToolbarOrderHistory();
//
//                mContract.setTitleToolbar("Home");
//                mContract.onLoadFragment(fragment);
//                break;
//
//            case R.id.navigation_cart:
//
//                load = false;
//                actionSearch.setVisible(false);
//                actionWishlist.setVisible(false);
//                actionHistory.setVisible(false);
//
//                toolbar.setTitle("Keranjang");
//                fragment = new CartFragment();
//                loadFragment(fragment);
//
//                break;
//
//            case R.id.navigation_order:
//
//                load = false;
//                actionSearch.setVisible(false);
//                actionWishlist.setVisible(false);
//                actionHistory.setVisible(true);
//
//                toolbar.setTitle("Dalam Proses");
//                fragment = new OrderFragment();
//                loadFragment(fragment);
//                break;
//
//            case R.id.navigation_profile:
//
//                load = false;
//                actionSearch.setVisible(false);
//                actionWishlist.setVisible(false);
//                actionHistory.setVisible(false);
//
//                toolbar.setTitle("Profil");
//                fragment = new ProfileFragment();
//                loadFragment(fragment);
//        }
//        return true
//    }
}
