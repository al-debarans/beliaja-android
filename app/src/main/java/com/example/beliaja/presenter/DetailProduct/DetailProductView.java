package com.example.beliaja.presenter.DetailProduct;

import com.example.beliaja.model.Produk;
import com.example.beliaja.model.Toko;

import java.util.List;

public interface DetailProductView {

    interface View{

        void displayToastError(int code, String onFailure);
        void displayProduk(List<Produk> produkDetail);
        void displayToko(List<Toko> tokoDetail);
        void displayProgressBar();
        void dismissProgressBar();
        void wishlistStatus(boolean status);
        void onWishlistHandler();
        void onMoreHandler();
        void onAddCartHandler();
        void onStoreHandler();
    }

    interface Presenter{

        void fetchProduk(String u_id, final String prod_id, String toko_id);
        void sendWishlist(String user_id, String produk_id, boolean status);
        void onStoreClick();
    }
}
