package com.example.beliaja.presenter.OrderList;

import com.example.beliaja.model.OrderList;

import java.util.List;

public interface OrderListView {

    interface View {

        void displayToastError(int code, String onFailure);
        void initAdapter(List<OrderList> orderList);
        void isEmpty(boolean status);
    }

    interface Presenter {
        void fetchOrderList(String order_id);
    }
}
