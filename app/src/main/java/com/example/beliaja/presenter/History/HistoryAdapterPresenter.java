package com.example.beliaja.presenter.History;

public class HistoryAdapterPresenter implements HistoryView.AdapterPresenter {

    private final HistoryView.AdapterView adapterView;

    public HistoryAdapterPresenter(HistoryView.AdapterView adapterView) {
        this.adapterView = adapterView;
    }

    @Override
    public void onCardViewClick(String order_id) {
        adapterView.onCardViewHandler(order_id);
    }
}
