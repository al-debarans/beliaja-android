package com.example.beliaja.presenter.Store;

import android.util.Log;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Produk;
import com.example.beliaja.model.Toko;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgProdukPresenter implements StoreView.PresenterProduk {

    private final StoreView.ViewProduk view;

    public FrgProdukPresenter(StoreView.ViewProduk view) {
        this.view = view;
    }

    @Override
    public void fetchStoreProduk(String toko_id) {
        produk(toko_id);
    }

    private void produk(String toko_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.getToko(toko_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Produk> produkList = response.body().getProduk();

                    view.initAdapterProduk(produkList);

                    if(produkList.size() > 0){
                        view.isEmpty(false);
                    }
                    else{
                        view.isEmpty(true);
                    }
                }
                else{
                    switch (response.code()) {
                        case 404:
                            view.displayToastError(404, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
    }
}
