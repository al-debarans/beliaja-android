package com.example.beliaja.presenter.Order;

import com.example.beliaja.model.Order;

import java.util.List;

public interface OrderView {

    interface View {

        void displayToastError(int code, String onFailure);
        void initAdapter(List<Order> orderOnProcess);
        void isEmpty(boolean status);
        void swipeRefreshEvent(boolean status);
        void refreshAdapter(boolean refresh);
    }

    interface Presenter {
        void fetchOrder(String user_id);
        void onRefreshAdapterEvent(boolean status);
    }

    interface AdapterView {
        void displayProgressBar();
        void dismissProgressBar();
        void refreshAdapter(boolean status);
        void displayToastError(int code, String onFailure);
        void cardViewHandler(String order_id);
    }

    interface AdapterPresenter {
        void sendCheckoutClick(String user_id, String order_id);
        void onCardViewClick(String order_id);
    }
}
