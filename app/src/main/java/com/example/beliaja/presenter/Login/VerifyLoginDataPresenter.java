package com.example.beliaja.presenter.Login;

import android.text.TextUtils;

import com.example.beliaja.base.Presenter;

public class VerifyLoginDataPresenter implements Presenter<LoginContract> {

    private LoginContract mContract;

    @Override
    public void onAttach(LoginContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

    private boolean isAllDataFilled(String email, String password) {

        return !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password);
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password){
        return password.length() < 5;
    }

    public void submitRegister(String email, String password) {
        if(isAllDataFilled(email, password)) {
            if(!isEmailValid(email)) {
                mContract.onShowToast("Wrong email format");
            }
            else if (isPasswordValid(password)) {
                mContract.onShowToast("Password char at least 5");
            }
            else {
                mContract.onPostLogin(email, password);
            }
        } else {
            mContract.onShowToast("All data must be filled");
        }
    }
}
