package com.example.beliaja.presenter.History;

import android.view.View;
import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Order;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryPresenter implements HistoryView.Presenter {

    private final HistoryView.View view;


    public HistoryPresenter(HistoryView.View view) {
        this.view = view;
    }

    @Override
    public void fetchHistory(String user_id) {
        history(user_id);
    }

    private void history(String user_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.getHistory(user_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Order> historyDetail = response.body().getOrder();

                    if(historyDetail.size() > 0){

                        view.dismissProgressBar();
                        view.displayHistory(historyDetail);
                        view.displayEmptyMsg(false);
                        view.setAdapter();

                    }
                    else{

                        view.displayEmptyMsg(true);
                        view.dismissProgressBar();
                    }
                }
                else{
                    switch (response.code()) {
                        case 400:
                            view.displayToastError(400, "");
                            break;
                        default:
                            view.displayToastError(0,"");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0,t.toString());
            }
        });

        view.swipeRefreshEvent(false);
    }
}
