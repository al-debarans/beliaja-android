package com.example.beliaja.presenter.Register;

import com.example.beliaja.base.Presenter;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter implements Presenter<RegisterContract> {

    private RegisterContract mContract;

    @Override
    public void onAttach(RegisterContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

    public void postRegister(String nama, String email, String password, String hp, String alamat){

        mContract.onShowProgressBar();
        mContract.onHideLayout();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postRegister( nama, email, password, hp, alamat );

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getResponse().equals("success")){
                        mContract.onHideProgressBar();
                        mContract.onShowLayout();
                        mContract.registerSuccess();
                    }
                }
                else {
                    // error case
                    mContract.onHideProgressBar();
                    mContract.onShowLayout();
                    switch (response.code()) {
                        case 401:
                            mContract.displayToastError("Bad request");
                            break;
                        case 409:
                            mContract.displayToastError("Email already use");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                mContract.displayToastError("Network Failure");
            }
        });
    }
}
