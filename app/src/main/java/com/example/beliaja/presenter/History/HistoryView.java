package com.example.beliaja.presenter.History;

import com.example.beliaja.model.Order;

import java.util.List;

public interface HistoryView {

    interface View{

        void displayToastError(int code, String onFailure);
        void displayHistory(List<Order> historyList);
        void displayProgressBar();
        void dismissProgressBar();
        void displayEmptyMsg(boolean status);
        void swipeRefreshEvent(boolean status);
        void setAdapter();
    }

    interface Presenter{
        void fetchHistory(String user_id);
    }

    interface AdapterView{
        void onCardViewHandler(String order_id);
    }

    interface AdapterPresenter{
        void onCardViewClick(String order_id);
    }
}
