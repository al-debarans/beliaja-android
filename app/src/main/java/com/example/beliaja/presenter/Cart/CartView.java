package com.example.beliaja.presenter.Cart;

import com.example.beliaja.model.Keranjang;

import java.util.List;

public interface CartView {

    interface View {
        void dismissProgressBar();
        void displayProgressBar();
        void setTotal(int total);
        void isEmpty(boolean status);
        void increaseHandler(String harga);
        void decreaseHandler(String harga);
        void refreshAdapter(boolean status);
        void isSuccessCheckout(boolean status);
        void swipeRefreshEvent(boolean status);
        void initAdapter(List<Keranjang> keranjangList);
        void displayToastError(int code, String onFailure);

    }
    interface Presenter {
        void sendCheckout(String user_id);
        void fetchKeranjang(String user_id);
        void onIncreaseClickEvent(String harga);
        void onDecreaseClickEvent(String harga);
        void onRefreshAdapterHandler(boolean status);
    }

    interface AdapterView {
        void dismissProgressBar();
        void displayProgressBar();
        void increaseHandler(String harga);
        void decreaseHandler(String harga);
        void refreshAdapter(boolean status);
        void removeSuccess(int position, int sizeOfObj);
        void displayToastError(int code, String onFailure);
        void textUbahHandler(String produk_id, String nama, String harga, String url_img);
        void textTitleHandler(String toko_id, String produk_id);
    }

    interface AdapterPresenter {
        void sendRemoveCart(String u_id, String prod_id, final int position, final int cartObjSize);
        void textUbahEvent(String produk_id, String nama, String harga, String url_img);
        void textTitleEvent(String toko_id, String produk_id);
        void onIncreaseEvent(String harga);
        void onDecreaseEvent(String harga);
    }
}
