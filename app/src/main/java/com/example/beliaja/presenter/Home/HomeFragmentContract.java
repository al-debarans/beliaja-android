package com.example.beliaja.presenter.Home;

import com.example.beliaja.base.Contract;
import com.example.beliaja.model.Produk;

import java.util.List;

public interface HomeFragmentContract extends Contract {

    void onSHowToast(String onFailure);
    void displayProduk(final List<Produk> produkList);
    void displayProgressBar();
    void dismissProgressBar();
    void onItemClickHandler(int position);
}
