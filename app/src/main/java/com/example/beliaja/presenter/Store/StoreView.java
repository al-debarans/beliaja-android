package com.example.beliaja.presenter.Store;

import com.example.beliaja.model.Produk;
import com.example.beliaja.model.Toko;

import java.util.List;

public interface StoreView {

    interface ViewStore {
        void displayToastError(int code, String onFailure);
        void isEmpty(boolean status);
        void initAdapterToko(List<Toko> tokoDetail);
    }

    interface PresenterStore {
        void fetchStoreDetail(String toko_id);
    }

    interface ViewProduk {
        void displayToastError(int code, String onFailure);
        void isEmpty(boolean status);
        void initAdapterProduk(List<Produk> produkList);
    }

    interface PresenterProduk {
        void fetchStoreProduk(String toko_id);
    }
}
