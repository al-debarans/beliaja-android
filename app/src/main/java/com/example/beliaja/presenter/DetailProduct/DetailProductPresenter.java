package com.example.beliaja.presenter.DetailProduct;

import android.util.Log;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Produk;
import com.example.beliaja.model.Toko;
import com.example.beliaja.model.Wishlist;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProductPresenter implements DetailProductView.Presenter {

    private final DetailProductView.View view;

    public DetailProductPresenter(DetailProductView.View view) {
        this.view = view;
    }

    public void onWishlistClick(){ view.onWishlistHandler(); }
    public void onMoreClick() { view.onMoreHandler(); }
    public void onAddCartButtonClick() { view.onAddCartHandler(); }

    @Override
    public void fetchProduk(String u_id, String prod_id, String toko_id) {
        produk(u_id, prod_id, toko_id);
    }

    @Override
    public void sendWishlist(String user_id, String produk_id, boolean status) {
        wishlist(user_id, produk_id, status);
    }

    @Override
    public void onStoreClick() {
        view.onStoreHandler();
    }

    private void produk(String u_id, final String prod_id, String toko_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.getProduk( u_id, prod_id,toko_id, "");

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Produk> produkDetail = response.body().getProduk();
                    final List<Toko> tokoDetail     = response.body().getToko();
                    final List<Wishlist> wishlist   = response.body().getWishlist();

                    view.displayToko(tokoDetail);
                    view.displayProduk(produkDetail);

                    if(wishlist.size() != 0){
                        view.wishlistStatus(true);
                    }else{
                        view.wishlistStatus(false);
                    }
                }
                else{
                    switch (response.code()) {
                        case 404:
                            view.displayToastError(404, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
    }

    private void wishlist(String uid, String produk_id, final boolean status){

        view.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call;
        if(status){
            call = apiInterface.postWishlist(uid, produk_id);
        }
        else{
            call = apiInterface.postUnwishlist(uid, produk_id);
        }

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getResponse().equals("success")){

                        if(status){
                            view.wishlistStatus(status);
                            view.dismissProgressBar();
                        }
                        else{
                            view.wishlistStatus(status);
                            view.dismissProgressBar();
                        }
                    }
                }
                else {
                    // error case
                    switch (response.code()) {
                        case 501:
                            view.displayToastError(5001, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });

    }
}
