package com.example.beliaja.presenter.Home;

import com.example.beliaja.base.Presenter;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Produk;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragmentPresenter implements Presenter<HomeFragmentContract> {

    private HomeFragmentContract mContract;

    @Override
    public void onAttach(HomeFragmentContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

    public void onItemClickEvent(int position) {
        mContract.onItemClickHandler(position);
    }

    public void produk(String prod_name){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);

        Call<CallResponse> call = apiInterface.getProduk("","", "", prod_name);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Produk> produkList = response.body().getProduk();

                    if(produkList.size() > 0){
                        mContract.dismissProgressBar();
                        mContract.displayProduk(produkList);
                    }
                }
                else{
                    mContract.dismissProgressBar();
                    if (response.code() == 404) {
                        mContract.onSHowToast("Not Found");
                    } else {
                        mContract.onSHowToast("Unknown error");
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                mContract.dismissProgressBar();
                mContract.onSHowToast("Network Failure");
            }
        });
    }
}
