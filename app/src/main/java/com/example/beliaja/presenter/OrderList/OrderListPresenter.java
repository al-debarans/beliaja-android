package com.example.beliaja.presenter.OrderList;

import android.view.View;
import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.OrderList;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderListPresenter implements OrderListView.Presenter {

    private final OrderListView.View view;

    public OrderListPresenter(OrderListView.View view) {
        this.view = view;
    }

    @Override
    public void fetchOrderList(String order_id) {
        orderList(order_id);
    }

    private void orderList(String order_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postOrderList(order_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<OrderList> orderList = response.body().getOrderlist();

                    if(orderList.size() > 0){
                        view.initAdapter(orderList);
                        view.isEmpty(false);

                    }
                    else{
                        view.isEmpty(true);
                    }
                }
                else{
                    switch (response.code()) {
                        case 400:
                            view.displayToastError(400, "");
                            break;
                        default:
                            view.displayToastError(0,"");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
    }
}
