package com.example.beliaja.presenter.AddCart;

public interface AddCartView {

    interface View{

        void displayToast(int code, String onFailure);
        void displayProgressBar();
        void dismissProgressBar();
        void setStatusCount(boolean status);
        void onHandleIncrease();
        void onHandleDecrease();
    }

    interface Presenter{
        void sendAddCart(String u_id, String prod_id, String jmlh, String notes);
    }
}
