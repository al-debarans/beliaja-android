package com.example.beliaja.presenter.Cart;

import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Keranjang;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartPresenter implements CartView.Presenter {

    private final CartView.View view;

    public CartPresenter(CartView.View view) {
        this.view = view;
    }

    @Override
    public void sendCheckout(String user_id) {
        checkout(user_id);
    }

    @Override
    public void fetchKeranjang(String user_id) {
        keranjang(user_id);
    }

    @Override
    public void onIncreaseClickEvent(String harga) {
        view.increaseHandler(harga);
    }

    @Override
    public void onDecreaseClickEvent(String harga) {
        view.decreaseHandler(harga);
    }

    @Override
    public void onRefreshAdapterHandler(boolean status) {
        view.refreshAdapter(status);
    }

    private void keranjang(String user_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.getKeranjang(user_id, "true");

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Keranjang> keranjangList = response.body().getKeranjang();
                    int total = 0;
                    view.initAdapter(keranjangList);

                    if(keranjangList.size() > 0){
                        view.isEmpty(false);

                        for(int i = 0; i < keranjangList.size(); i++){

                            int harga = Integer.valueOf(keranjangList.get(i).getProduk_harga().replace(".", ""));
                            short jmlh = Short.valueOf(keranjangList.get(i).getJumlah());
                            int hargaTot = harga * jmlh;
                            total = total + hargaTot;
                        }

                        view.setTotal(total);
                    }
                    else{

                        view.isEmpty(true);
                    }
                }
                else{
                    switch (response.code()) {
                        case 400:
                            view.displayToastError(400, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });

        view.swipeRefreshEvent(false);
    }

    private void checkout(final String user_id){

        view.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postOrder(user_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){
                    view.isSuccessCheckout(true);
                    keranjang(user_id);
                }
                else{
                    switch (response.code()) {
                        case 501:
                            view.displayToastError(501, "");
                            break;
                        default:
                            view.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToastError(0, t.toString());
            }
        });
    }
}
