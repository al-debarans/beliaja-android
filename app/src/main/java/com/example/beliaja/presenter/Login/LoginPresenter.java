package com.example.beliaja.presenter.Login;

import android.util.Log;

import com.example.beliaja.base.Presenter;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Pembeli;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements Presenter<LoginContract> {

    private LoginContract mContract;

    @Override
    public void onAttach(LoginContract contract) {
        mContract = contract;
    }

    @Override
    public void onDettach() {
        mContract = null;
    }

    public void auth(final String email, final String password){

        mContract.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postLogin(email, password);
        Log.e("req", "test");
        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {

                if(response.isSuccessful()){
                    if(response.body().getResponse().equals("success")){

                        mContract.dismissProgressBar();
                        final List<Pembeli> profileDetail = response.body().getProfile();
                        String uid = "";
                        for(int i = 0; i < profileDetail.size(); i++){

                            uid = profileDetail.get(i).getPembeli_id();
                        }
                        mContract.successLogin(uid, email,password);
                    }
                }
                else {
                    mContract.dismissProgressBar();
                    // error case
                    if (response.code() == 401) {
                        mContract.onShowToast("Bad Request");
                    } else {
                        mContract.onShowToast("Unknown Error");
                    }
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                mContract.dismissProgressBar();
                mContract.onShowToast("Network Failure");
            }
        });
    }
}
