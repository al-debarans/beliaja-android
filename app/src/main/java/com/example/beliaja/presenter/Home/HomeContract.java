package com.example.beliaja.presenter.Home;

import android.support.v4.app.Fragment;

import com.example.beliaja.base.Contract;

public interface HomeContract extends Contract {

    void onLoadFragment(Fragment fragment);
    void showToolbarSearch();
    void hideToolbarSearch();
    void showToolbarWishlist();
    void hideToolbarWIshlist();
    void showToolbarOrderHistory();
    void hideToolbarOrderHistory();
    void setTitleToolbar(String title);
}
