package com.example.beliaja.presenter.Register;

import com.example.beliaja.base.Contract;

public interface RegisterContract extends Contract {

    void onShowProgressBar();
    void onHideProgressBar();
    void onShowLayout();
    void onHideLayout();
    void registerSuccess();

    void onShowToast(String message);
    void displayToastError(String onFailure);
    void onPostRegister(String nama, String email, String password, String phone, String alamat);
}
