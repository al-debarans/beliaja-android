package com.example.beliaja.presenter.Wishlist;

import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistAdapterPresenter implements WishlistView.AdapterPresenter{

    private final WishlistView.AdapterVIew adapterVIew;

    public WishlistAdapterPresenter(WishlistView.AdapterVIew adapterVIew) {
        this.adapterVIew = adapterVIew;
    }

    @Override
    public void sendWishlistStatus(String user_id, String produk_id, int position, int sizeOfObj) {
        unwishlist(user_id, produk_id, position, sizeOfObj);
    }

    @Override
    public void btnBuyClickEvent(String prod_id, String prod_nama, String harga, String url) {
        adapterVIew.btnBuyHandler(prod_id, prod_nama, harga, url);
    }

    @Override
    public void cardClickEvent(String toko_id, String prod_id) {
        adapterVIew.cardHandler(toko_id, prod_id);
    }

    private void unwishlist(String user_id, String produk_id, final int position, final int sizeOfObj){

        adapterVIew.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postUnwishlist(user_id, produk_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    adapterVIew.removeSuccess(position, sizeOfObj);
                }
                else{
                    switch (response.code()) {
                        case 404:
                            adapterVIew.displayToastError(404, "");
                            break;
                        default:
                            adapterVIew.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                adapterVIew.displayToastError(0, t.toString());
            }
        });
    }
}
