package com.example.beliaja.presenter.Profile;

import com.example.beliaja.model.Pembeli;

import java.util.List;

public interface ProfileView {

    interface View {
        void dismissProgressBar();
        void displayProfile(List<Pembeli> pembeliList);
        void displayToastError(int code, String onFailure);
    }

    interface Presenter {
        void fetchProfile(String email);
    }
}
