package com.example.beliaja.presenter.Wishlist;

import android.view.View;
import android.widget.Toast;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Wishlist;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishlistPresenter implements WishlistView.Presenter {

    private final WishlistView.View view;

    public WishlistPresenter(WishlistView.View view) {
        this.view = view;
    }

    @Override
    public void fetchWishlist(String user_id) {
        wishlist((user_id));
    }

    private void wishlist(String user_id){

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.getWishlist(user_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    final List<Wishlist> wishlistList = response.body().getWishlist();
                    view.initAdapter(wishlistList);

                    if(wishlistList.size() > 0){
                        view.isEmpty(false);
                    }
                    else{
                        view.isEmpty(true);
                    }
                }
                else{
                    switch (response.code()) {
                        case 400:
                            view.displayToast(400, "");
                            break;
                        default:
                            view.displayToast(0, "");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToast(0, t.toString());
            }
        });

        view.swipeRefreshEvent(false);
    }
}
