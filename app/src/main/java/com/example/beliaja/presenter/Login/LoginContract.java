package com.example.beliaja.presenter.Login;

import com.example.beliaja.base.Contract;

public interface LoginContract extends Contract {

    void displayProgressBar();
    void dismissProgressBar();
    void onShowToast(String message);
    void onPostLogin(String email, String password);
    void successLogin(String user_id, String email, String password);
}
