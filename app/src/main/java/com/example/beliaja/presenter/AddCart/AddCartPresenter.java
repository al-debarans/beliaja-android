package com.example.beliaja.presenter.AddCart;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCartPresenter implements AddCartView.Presenter {

    private int jumlah;
    private final AddCartView.View view;

    public AddCartPresenter(AddCartView.View view) {
        this.view = view;
    }

    public void setDecStatus(boolean status){
        view.setStatusCount(status);
    }

    public void onIncreaseClick(){
        view.onHandleIncrease();
    }

    public void onDecreaseClick(){
        view.onHandleDecrease();
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public int increase(int harga){
        jumlah = jumlah + harga;
        return jumlah;
    }

    public int derease(int harga){
        jumlah = jumlah - harga;
        return jumlah;
    }

    private void addCart(String u_id, String prod_id, String jmlh, String notes){

        view.displayProgressBar();

        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postKeranjangAdd(u_id, prod_id, jmlh, notes);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){

                    view.dismissProgressBar();
                }
                else{
                    switch (response.code()) {
                        case 501:
                            view.displayToast(501, null);
                            break;
                        default:
                            view.displayToast(0, null);
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                view.displayToast(0, t.toString());
            }
        });
    }

    @Override
    public void sendAddCart(String u_id, String prod_id, String jmlh, String notes) {
        addCart(u_id,prod_id,jmlh,notes);
    }
}
