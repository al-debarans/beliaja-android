package com.example.beliaja.presenter.Order;

import com.example.beliaja.model.CallResponse;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAdapterPresenter implements OrderView.AdapterPresenter {

    private final OrderView.AdapterView adapterView;

    public OrderAdapterPresenter(OrderView.AdapterView adapterView) {
        this.adapterView = adapterView;
    }

    @Override
    public void sendCheckoutClick(String user_id, String order_id) {
        bayar(user_id, order_id);
    }

    @Override
    public void onCardViewClick(String order_id) {
        adapterView.cardViewHandler(order_id);
    }

    private void bayar(String user_id, String order_id){

        adapterView.displayProgressBar();
        ApiInterface apiInterface = ApiClient.getUrl().create(ApiInterface.class);
        Call<CallResponse> call = apiInterface.postBayar(user_id, order_id);

        call.enqueue(new Callback<CallResponse>() {
            @Override
            public void onResponse(Call<CallResponse> call, Response<CallResponse> response) {
                if(response.isSuccessful()){
                    adapterView.refreshAdapter(true);
                    adapterView.dismissProgressBar();
                }
                else{
                    switch (response.code()) {
                        case 400:
                            adapterView.dismissProgressBar();
                            adapterView.displayToastError(400, "");
                            break;
                        default:
                            adapterView.dismissProgressBar();
                            adapterView.displayToastError(0, "");
                            break;
                    }
                }

            }

            @Override
            public void onFailure(Call<CallResponse> call, Throwable t) {
                adapterView.displayToastError(0,t.toString());
            }
        });
    }
}
