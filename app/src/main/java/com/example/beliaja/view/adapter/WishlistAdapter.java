package com.example.beliaja.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Wishlist.WishlistAdapterPresenter;
import com.example.beliaja.presenter.Wishlist.WishlistView;
import com.example.beliaja.view.activity.AddCartActivity;
import com.example.beliaja.view.activity.DetailProductActivity;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Wishlist;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.DialogBuilder;
import com.example.beliaja.utility.ImgDownloader;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> implements WishlistView.AdapterVIew {
    Context context;
    SessionManager session;
    List<Wishlist> wishlists;
    DialogBuilder dialogBuilder;
    HashMap<String, String> profile;
    WishlistAdapterPresenter presenter;

    public WishlistAdapter(Context context, List<Wishlist> wishlists) {
        this.context = context;
        this.wishlists = wishlists;
        this.session = new SessionManager(context);
        this.profile = session.getData();
        presenter = new WishlistAdapterPresenter(this);
        dialogBuilder = new DialogBuilder();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.wish_card)           CardView cardView;
        @BindView(R.id.wish_adptr_title)    TextView txt_title;
        @BindView(R.id.wish_adptr_price)    TextView txt_price;
        @BindView(R.id.wish_adptr_store)    TextView txt_store;
        @BindView(R.id.wish_adptr_img)      ImageView img_cover;
        @BindView(R.id.wish_adptr_btndel)   Button btn_del;
        @BindView(R.id.wish_adptr_btnbuy)   Button btn_buy;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public WishlistAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_wishlist,viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        final Wishlist mWishlist = wishlists.get(position);

        viewHolder.txt_store.setText(mWishlist.getToko_nama());
        viewHolder.txt_title.setText(mWishlist.getProduk_nama());
        viewHolder.txt_price.setText(mWishlist.getProduk_harga());

        final String url = Constant.PROD_URL + mWishlist.getProduk_pict();
        ImgDownloader.downloadAndSetImg(url, viewHolder.img_cover);

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.cardClickEvent(mWishlist.getToko_id(), mWishlist.getProduk_id());
            }
        });

        viewHolder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.sendWishlistStatus(
                        profile.get(session.USER_ID), mWishlist.getProduk_id(),
                        position, wishlists.size()
                );
            }
        });

        viewHolder.btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.btnBuyClickEvent(
                        mWishlist.getProduk_id(), mWishlist.getProduk_nama(),
                        mWishlist.getProduk_harga(), url);
            }
        });

    }

    @Override
    public int getItemCount() {
        return wishlists.size();
    }

    @Override
    public void removeSuccess(int position, int sizeofObj) {
        dialogBuilder.dismissPrg();
        Toast.makeText(context, "Wishlist dihapus", Toast.LENGTH_SHORT).show();

        // posisi item yang dihapus
        wishlists.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, wishlists.size());

    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(context, onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 404){
            Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(context);
    }

    @Override
    public void btnBuyHandler(String prod_id, String prod_nama, String harga, String url) {
        Intent intent = new Intent(context, AddCartActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("prod_id", prod_id);
        bundle.putString("prod_title", prod_nama);
        bundle.putString("prod_price", harga);
        bundle.putString("prod_img", url);

        Log.e("_img_url", url);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }

    @Override
    public void cardHandler(String toko_id, String prod_id) {
        Intent intent = new Intent(context, DetailProductActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toko_id", toko_id);
        bundle.putString("produk_id", prod_id);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }

}