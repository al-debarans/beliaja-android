package com.example.beliaja.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.presenter.Register.RegisterPresenter;
import com.example.beliaja.presenter.Register.RegisterContract;
import com.example.beliaja.presenter.Register.VerifyDataRegisterPresenter;
import com.example.beliaja.utility.DialogBuilder;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity implements RegisterContract {

    SessionManager session;
    DialogBuilder dialogBuilder;
    private RegisterPresenter requestPresenter;
    private VerifyDataRegisterPresenter verifyPresenter;
    HashMap<String, String> users;

    @BindView(R.id.reg_nama)        EditText txt_nama;
    @BindView(R.id.reg_email)       EditText txt_email;
    @BindView(R.id.reg_alamat)      EditText txt_alamat;
    @BindView(R.id.reg_hp)          EditText txt_hp;
    @BindView(R.id.reg_scroolview)  ScrollView scrollView;
    @BindView(R.id.reg_password)    EditText txt_password;

    @OnClick(R.id.reg_loginTxt) void gotoLogin(){
        Intent intent = new Intent(
                RegisterActivity.this, LoginActivity.class
        );
        startActivity(intent);
    }

    @OnClick(R.id.reg_btnSubmit) void submit(){
        verifyPresenter.submitRegister(
                txt_nama.getText().toString(), txt_email.getText().toString(),
                txt_hp.getText().toString(), txt_alamat.getText().toString(),
                txt_password.getText().toString()

        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setOnMain();

        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void setOnMain(){
        dialogBuilder = new DialogBuilder();
        session = new SessionManager(RegisterActivity.this);
        ButterKnife.bind(this);
        initPresenter();
        onAttachView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        session = new SessionManager(this);
        users = session.getData();
        if (users.get(session.EMAIL) != null && users.get(session.PASSWORD) != null){
            finish();
        }
    }

    private void clear(){
        txt_nama.setText("");
        txt_email.setText("");
        txt_alamat.setText("");
        txt_hp.setText("");
        txt_password.setText("");
    }

    private void initPresenter(){
        requestPresenter = new RegisterPresenter();
        verifyPresenter = new VerifyDataRegisterPresenter();
    }

    @Override
    public void onAttachView() {
        requestPresenter.onAttach(this);
        verifyPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        requestPresenter.onDettach();
        verifyPresenter.onDettach();
    }

    @Override
    protected void onDestroy() {
        onDetachView();
        super.onDestroy();
    }

    @Override
    public void onShowProgressBar() {
        dialogBuilder.showDialogPrg(RegisterActivity.this);
    }

    @Override
    public void onHideProgressBar() {
        dialogBuilder.dismissPrg();
    }

    @Override
    public void onShowLayout() {
        scrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLayout() {
        scrollView.setVisibility(View.GONE);
    }

    @Override
    public void onPostRegister(String nama, String email, String password, String phone, String alamat) {
        requestPresenter.postRegister(nama, email, password, phone, alamat);
    }

    @Override
    public void registerSuccess() {
        clear();
        dialogBuilder.showDialogRegSuccess(RegisterActivity.this);
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayToastError(String onFailure) {
        Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
    }
}
