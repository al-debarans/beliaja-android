package com.example.beliaja.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beliaja.R;
import com.example.beliaja.model.Produk;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.ImgDownloader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentProdukAdapter extends RecyclerView.Adapter<FragmentProdukAdapter.ViewHolder> {

    Context context;
    List<Produk> produkList;

    public FragmentProdukAdapter(Context context, List<Produk> produkList) {
        this.context = context;
        this.produkList = produkList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.strfrg_img)      ImageView img_prod;
        @BindView(R.id.strfrg_title)    TextView txt_title;
        @BindView(R.id.strfrg_price)    TextView txt_price;
        @BindView(R.id.strfrg_wishlist) CircleImageView img_wish;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public FragmentProdukAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_frg_produk,viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Produk mProduk = produkList.get(position);

        viewHolder.txt_title.setText( mProduk.getProduk_nama() );
        viewHolder.txt_price.setText( mProduk.getProduk_harga() );
        String url = Constant.PROD_URL + mProduk.getProduk_pict();
        ImgDownloader.downloadAndSetImg(url,viewHolder.img_prod);
        
    }

    @Override
    public int getItemCount() {
        return produkList.size();
    }
}
