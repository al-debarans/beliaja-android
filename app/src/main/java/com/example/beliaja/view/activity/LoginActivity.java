package com.example.beliaja.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.presenter.Login.LoginPresenter;
import com.example.beliaja.presenter.Login.LoginContract;
import com.example.beliaja.presenter.Login.VerifyLoginDataPresenter;
import com.example.beliaja.utility.DialogBuilder;
import com.xwray.passwordview.PasswordView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements  LoginContract {

    private SessionManager session;
    private HashMap<String, String> users;
    private DialogBuilder dialogBuilder;
    private LoginPresenter loginPresenter;
    private VerifyLoginDataPresenter verifyPresenter;

    @BindView(R.id.login_email)     EditText txt_email;
    @BindView(R.id.login_password)  EditText txt_password;

    @OnClick(R.id.login_signupTxt) void signUp(){
        Intent intent = new Intent(
                LoginActivity.this, RegisterActivity.class
        );
        startActivity(intent);
    }

    @OnClick(R.id.login_btn) void login(){
        verifyPresenter.submitRegister(txt_email.getText().toString(), txt_password.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setOnCreate();

        getSupportActionBar().setTitle("Login");

    }

    private void setOnCreate() {

        session = new SessionManager(LoginActivity.this);
        session.checkExist();
        dialogBuilder  = new DialogBuilder();
        ButterKnife.bind(this);
        initPresenter();
        onAttachView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {

        super.onResume();
        session = new SessionManager(this);
        users = session.getData();
        if (users.get(session.EMAIL) != null && users.get(session.PASSWORD) != null){
            finish();
        }
    }

    private void initPresenter(){
        verifyPresenter = new VerifyLoginDataPresenter();
        loginPresenter = new LoginPresenter();
    }

    @Override
    public void onAttachView() {
        verifyPresenter.onAttach(this);
        loginPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        verifyPresenter.onDettach();
        loginPresenter.onDettach();
    }

    @Override
    protected void onDestroy() {
        onDetachView();
        super.onDestroy();
    }

    @Override
    public void displayProgressBar() {

        dialogBuilder.showDialogLoading(LoginActivity.this);
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPostLogin(String email, String password) {
        loginPresenter.auth(email, password);
    }

    @Override
    public void successLogin(String user_id, String email, String password) {
        session.createSession(user_id, email, password);
        startActivity(new Intent(
                LoginActivity.this, MainActivity.class
        ));

        finish();
    }
}
