package com.example.beliaja.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Produk;
import com.example.beliaja.model.Toko;
import com.example.beliaja.presenter.DetailProduct.DetailProductPresenter;
import com.example.beliaja.presenter.DetailProduct.DetailProductView;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.DialogBuilder;
import com.example.beliaja.utility.ImgDownloader;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailProductActivity extends AppCompatActivity implements DetailProductView.View {

    Bundle bundle;
    SessionManager session;
    boolean wishlistStatus;
    DialogBuilder dialogBuilder;
    HashMap<String, String> profile;
    DetailProductPresenter presenter;
    String toko_id, produk_id, url_img, url_toko, toko_des, toko_rat, toko_start;

    @BindView(R.id.detail_title)    TextView txt_title;
    @BindView(R.id.detail_price)    TextView txt_price;
    @BindView(R.id.detail_stok)     TextView txt_stok;
    @BindView(R.id.detail_descript) TextView txt_descrpt;
    @BindView(R.id.detail_store)    TextView txt_store;
    @BindView(R.id.detail_loc)      TextView txt_loc;
    @BindView(R.id.detail_lengkap)  TextView txt_more;
    @BindView(R.id.detail_img)      ImageView img_prod;
    @BindView(R.id.fab)             FloatingActionButton fab;

    @OnClick(R.id.detail_tokoprof) void storeDetail(){
        presenter.onStoreClick();
    }

    @OnClick(R.id.detail_btncart)
    void addCart(){
        presenter.onAddCartButtonClick();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setOnCreate();

        getSupportActionBar().setTitle("Produk Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onWishlistClick();
            }
        });

        txt_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onMoreClick();
            }
        });
    }

    private void setOnCreate(){

        bundle = getIntent().getExtras();
        toko_id = bundle.getString("toko_id");
        produk_id = bundle.getString("produk_id");

        session = new SessionManager(this);
        profile = session.getData();
        dialogBuilder = new DialogBuilder();

        ButterKnife.bind(this);
        presenter = new DetailProductPresenter(this);
        presenter.fetchProduk(profile.get(session.USER_ID),produk_id, toko_id);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
        }
        else if(code == 404){
            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_SHORT).show();
        }
        else if(code == 501){
            Toast.makeText(getApplicationContext(), "Bad Request", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Unknown error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayProduk(List<Produk> produkDetail) {

        for(int i = 0; i < produkDetail.size(); i++){

            txt_price.setText(produkDetail.get(i).getProduk_harga());
            txt_title.setText(produkDetail.get(i).getProduk_nama());
            txt_stok.setText(produkDetail.get(i).getProduk_stock());
            txt_descrpt.setText(produkDetail.get(i).getProduk_deskripsi());
            url_img = Constant.PROD_URL + produkDetail.get(i).getProduk_pict();
            ImgDownloader.downloadAndSetImg(url_img, img_prod);
        }
    }

    @Override
    public void displayToko(List<Toko> tokoDetail) {
        for(int i = 0; i < tokoDetail.size(); i++){
            txt_store.setText(tokoDetail.get(i).getToko_nama());
            txt_loc.setText(tokoDetail.get(i).getToko_lokasi());
            url_toko = tokoDetail.get(i).getToko_pict();
            toko_des = tokoDetail.get(i).getToko_deskripsi();
            toko_rat = tokoDetail.get(i).getToko_reputasi();
            toko_start = tokoDetail.get(i).getToko_created();
        }
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(DetailProductActivity.this);
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissPrg();
    }

    @Override
    public void wishlistStatus(boolean status) {

        if(status){
            wishlistStatus = false;
            fab.setImageResource(R.drawable.ic_wishlist);
        } else {
            wishlistStatus = true;
            fab.setImageResource(R.drawable.ic_unwishlist);
        }
    }

    @Override
    public void onWishlistHandler() {
        presenter.sendWishlist(profile.get(session.USER_ID), produk_id, wishlistStatus);
    }

    @Override
    public void onMoreHandler() {
        if(txt_more.getText().toString().equals("Baca Selengkapnya")){
            txt_more.setText("Tampilkan Sedikit");
            txt_descrpt.setSingleLine(false);
        }
        else {
            txt_more.setText("Baca Selengkapnya");
            txt_descrpt.setSingleLine(true);
        }
    }

    @Override
    public void onAddCartHandler() {

        Intent intent = new Intent(DetailProductActivity.this, AddCartActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("prod_id", produk_id);
        bundle.putString("prod_title", txt_title.getText().toString());
        bundle.putString("prod_price", txt_price.getText().toString());
        bundle.putString("prod_img", url_img);

        intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    public void onStoreHandler() {
        Intent intent = new Intent(DetailProductActivity.this, StoreActivity.class);
        Bundle bundle = new Bundle();

        String[] store = {toko_id, txt_store.getText().toString(), url_toko, toko_des,
                                                                                            txt_loc.getText().toString(), toko_rat, toko_start};

        bundle.putStringArray("store", store);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
