package com.example.beliaja.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beliaja.R;
import com.example.beliaja.model.OrderList;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.DateConverter;
import com.example.beliaja.utility.ImgDownloader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.ViewHolder> {

    Context context;
    List<OrderList> orderList;

    public OrderListAdapter(Context context, List<OrderList> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ordl_title)  TextView txt_title;
        @BindView(R.id.ordl_date)   TextView txt_date;
        @BindView(R.id.ordl_jmlh)   TextView txt_jumlah;
        @BindView(R.id.ordl_img)    ImageView img_cover;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public OrderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_order_list,viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        final OrderList mOrderList = orderList.get(position);

        viewHolder.txt_title.setText(mOrderList.getProduk_nama());
        viewHolder.txt_date.setText(DateConverter.convert(mOrderList.getCreated()));
        viewHolder.txt_jumlah.setText(mOrderList.getJumlah());
        String url = Constant.PROD_URL + mOrderList.getProduk_pict();
        ImgDownloader.downloadAndSetImg(url, viewHolder.img_cover);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
