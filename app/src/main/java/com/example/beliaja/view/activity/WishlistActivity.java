package com.example.beliaja.view.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Wishlist.WishlistPresenter;
import com.example.beliaja.presenter.Wishlist.WishlistView;
import com.example.beliaja.view.adapter.WishlistAdapter;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Wishlist;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WishlistActivity extends AppCompatActivity implements WishlistView.View {

    SessionManager session;
    WishlistAdapter wishlistAdapter;
    HashMap<String, String> profile;
    WishlistView.Presenter presenter;

    @BindView(R.id.wish_progress)    ProgressBar progressBar;
    @BindView(R.id.wish_recycle)     RecyclerView recyclerView;
    @BindView(R.id.wish_notfound)    TextView txt_empty;
    @BindView(R.id.wish_swipe)       SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        setOnMain();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setAdapter(null);
                presenter.fetchWishlist(profile.get(session.USER_ID));
            }
        });
    }

    private void setOnMain(){

        session         = new SessionManager(this);
        profile         = session.getData();

        presenter = new WishlistPresenter(this);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Wishlist Anda");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter.fetchWishlist(profile.get(session.USER_ID));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.fetchWishlist(profile.get(session.USER_ID));
    }

    @Override
    public void initAdapter(List<Wishlist> wishlistsList) {
        wishlistAdapter = new WishlistAdapter(this, wishlistsList);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void isEmpty(boolean status) {
        progressBar.setVisibility(View.GONE);
        if(status){
            txt_empty.setVisibility(View.VISIBLE);
        } else {
            txt_empty.setVisibility(View.GONE);
            recyclerView.setAdapter(wishlistAdapter);
        }
    }

    @Override
    public void displayToast(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 400){
            Toast.makeText(getApplicationContext(), "Bad Request", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void swipeRefreshEvent(boolean status) {

        swipeRefresh.setRefreshing(status);
    }

}