package com.example.beliaja.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Order.OrderPresenter;
import com.example.beliaja.presenter.Order.OrderView;
import com.example.beliaja.view.adapter.OrderAdapter;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Order;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends Fragment implements OrderView.View {

    SessionManager session;
    OrderAdapter orderAdapter;
    OrderPresenter presenter;
    HashMap<String, String> profile;

    @BindView(R.id.order_empty)     TextView txt_empty;
    @BindView(R.id.order_recycle)   RecyclerView recyclerView;
    @BindView(R.id.order_swipe)     SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.order_progress)  ProgressBar progressBar;

    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, view);

        session = new SessionManager(getActivity());
        profile = session.getData();
        presenter = new OrderPresenter(this);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setAdapter(null);
                presenter.fetchOrder(profile.get(session.USER_ID));
            }
        });

        presenter.fetchOrder(profile.get(session.USER_ID));

        return view;
    }

    @Override
    public void initAdapter(List<Order> orderOnProcess) {
        orderAdapter = new OrderAdapter(getActivity(), orderOnProcess, presenter);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getActivity(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 400){
            Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void isEmpty(boolean status) {

        progressBar.setVisibility(View.GONE);
        if(status){
            txt_empty.setVisibility(View.VISIBLE);
        } else {
            txt_empty.setVisibility(View.GONE);
            recyclerView.setAdapter(orderAdapter);
        }
    }

    @Override
    public void swipeRefreshEvent(boolean status) {
        swipeRefresh.setRefreshing(status);
    }

    @Override
    public void refreshAdapter(boolean refresh) {
        if(refresh){
            recyclerView.setAdapter(null);
            presenter.fetchOrder(profile.get(session.USER_ID));
        }
    }
}
