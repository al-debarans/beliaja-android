package com.example.beliaja.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.presenter.AddCart.AddCartPresenter;
import com.example.beliaja.presenter.AddCart.AddCartView;
import com.example.beliaja.utility.DialogBuilder;
import com.example.beliaja.utility.ImgDownloader;

import java.text.DecimalFormat;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AddCartActivity extends AppCompatActivity implements AddCartView.View {

    int total;
    Bundle bundle;
    DecimalFormat df;
    String prod_id, price;
    SessionManager session;
    AddCartPresenter presenter;
    DialogBuilder dialogBuilder;
    HashMap<String, String> profile;

    @BindView(R.id.addc_title)  TextView txt_title;
    @BindView(R.id.addc_price)  TextView txt_price;
    @BindView(R.id.addc_total)  TextView txt_total;
    @BindView(R.id.addc_cover)  ImageView img_cover;
    @BindView(R.id.addc_count)  EditText ed_count;
    @BindView(R.id.addc_notes)  EditText ed_notes;
    @BindView(R.id.addc_dec)    CircleImageView img_dec;

    @OnClick(R.id.addc_inc)
    void increase(){
        presenter.onIncreaseClick();
    }

    @OnClick(R.id.addc_dec)
    void decrease(){
        presenter.onDecreaseClick();
    }

    @OnClick(R.id.addc_btnAdd)
    void clickAddCart(){
        String jumlah = ed_count.getText().toString();
        String notes = ed_notes.getText().toString();

        presenter.sendAddCart(profile.get(session.USER_ID), prod_id, jumlah, notes);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setOnCreate();
    }

    private void setOnCreate(){

        bundle = getIntent().getExtras();
        prod_id = bundle.getString("prod_id");
        price = bundle.getString("prod_price");

        String title = bundle.getString("prod_title");
        String url_img = bundle.getString("prod_img");
        df = new DecimalFormat("#,###");

        ButterKnife.bind(this);
        presenter = new AddCartPresenter(this);
        // session read
        session = new SessionManager(this);
        profile = session.getData();

        dialogBuilder = new DialogBuilder();

        txt_title.setText(title);
        txt_price.setText(price);
        txt_total.setText(price);

        presenter.setJumlah(Integer.valueOf(price.replace(".", "")));
        ImgDownloader.downloadAndSetImg(url_img, img_cover);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void displayToast(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
        }
        else if(code == 501){
            Toast.makeText(getApplicationContext(), "Bad Request", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(), "Unknown error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(AddCartActivity.this);
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissPrg();
        finish();
    }

    @Override
    public void setStatusCount(boolean status) {

        if(status){
            img_dec.setBorderColor(-1086464);
            img_dec.setImageResource(R.mipmap.ic_decrease);
        }
        else {
            img_dec.setBorderColor(-7566196);
            img_dec.setImageResource(R.mipmap.ic_dec_gray);
        }
    }

    @Override
    public void onHandleIncrease() {

        presenter.setDecStatus(true);
        total = presenter.increase(
                Integer.valueOf(price.replace(".", ""))
        );

        int cnt = Integer.valueOf(ed_count.getText().toString()) + 1;

        ed_count.setText(String.valueOf(cnt));
        txt_total.setText(df.format(total).replace(",", "."));
    }

    @Override
    public void onHandleDecrease() {

        int cnt = Integer.valueOf(ed_count.getText().toString()) - 1;

        if(cnt < 1){
            cnt = 1;
        }
        else if (cnt == 1){

            presenter.setDecStatus(false);
            total = presenter.derease(
                    Integer.valueOf(price.replace(".", ""))
            );
        }
        else{
            total = presenter.derease(
                    Integer.valueOf(price.replace(".", ""))
            );
        }
        ed_count.setText(String.valueOf(cnt));
        txt_total.setText(df.format(total).replace(",", "."));
    }
}
