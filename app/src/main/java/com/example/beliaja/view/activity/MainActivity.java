package com.example.beliaja.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.view.fragment.CartFragment;
import com.example.beliaja.view.fragment.OrderFragment;
import com.example.beliaja.view.fragment.HomeFragment;
import com.example.beliaja.view.fragment.ProfileFragment;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    boolean load = true;
    SessionManager session;
    private ActionBar toolbar;
    HashMap<String, String> data;
    MenuItem actionSearch, actionWishlist, actionHistory;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment fragment;

            switch (item.getItemId()) {
                case R.id.navigation_home:

                    load = false;
                    actionSearch.setVisible(true);
                    actionWishlist.setVisible(true);
                    actionHistory.setVisible(false);

                    toolbar.setTitle("Home");
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;

                case R.id.navigation_cart:

                    load = false;
                    actionSearch.setVisible(false);
                    actionWishlist.setVisible(false);
                    actionHistory.setVisible(false);

                    toolbar.setTitle("Keranjang");
                    fragment = new CartFragment();
                    loadFragment(fragment);

                    return true;

                case R.id.navigation_order:

                    load = false;
                    actionSearch.setVisible(false);
                    actionWishlist.setVisible(false);
                    actionHistory.setVisible(true);

                    toolbar.setTitle("Dalam Proses");
                    fragment = new OrderFragment();
                    loadFragment(fragment);

                    return true;

                case R.id.navigation_profile:

                    load = false;
                    actionSearch.setVisible(false);
                    actionWishlist.setVisible(false);
                    actionHistory.setVisible(false);

                    toolbar.setTitle("Profil");
                    fragment = new ProfileFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();

        session = new SessionManager(this);
        data = session.getData();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if(load){
            toolbar.setTitle("Home");
            loadFragment(new HomeFragment());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        session = new SessionManager(this);
        data = session.getData();
        if (data.get(session.EMAIL) == null){
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        actionSearch    = menu.findItem(R.id.action_search);
        actionWishlist  = menu.findItem(R.id.action_wishlist);
        actionHistory   = menu.findItem(R.id.action_history);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Toast.makeText(this,"Search", Toast.LENGTH_SHORT).show();
            return true;
        }
        else if (id == R.id.action_wishlist){
            Intent intent = new Intent(
                    MainActivity.this, WishlistActivity.class
            );
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_history){
            Intent intent = new Intent(
                    MainActivity.this, HistoryActivity.class
            );
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
