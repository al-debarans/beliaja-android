package com.example.beliaja.view.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
;
import com.example.beliaja.R;
import com.example.beliaja.model.Produk;
import com.example.beliaja.presenter.Store.FrgProdukPresenter;
import com.example.beliaja.presenter.Store.StoreView;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.ImgDownloader;
import com.example.beliaja.view.fragment.StoreFragment.InfoFragment;
import com.example.beliaja.view.fragment.StoreFragment.ProdukFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreActivity extends AppCompatActivity {

    Bundle bundle;

    @BindView(R.id.store_viewpager)     ViewPager viewPager;
    @BindView(R.id.store_tab)           TabLayout tabLayout;
    @BindView(R.id.store_name)          TextView txt_store;
    @BindView(R.id.store_img)           ImageView img_store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        ButterKnife.bind(this);

        bundle = getIntent().getExtras();
        String[] dataStore = bundle.getStringArray("store");
        txt_store.setText(dataStore[1]);
        initBundleFragment(myPagerAdapter, dataStore);
        viewPager.setAdapter(myPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        String url_img = Constant.STORE_URL + dataStore[2];
        ImgDownloader.downloadAndSetImg(url_img, img_store);

        getSupportActionBar().setTitle(dataStore[1]);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initBundleFragment(MyPagerAdapter myPagerAdapter, String[] store){
        Bundle bundle = new Bundle();
        bundle.putStringArray("data", store);

        ProdukFragment prodObj = new ProdukFragment();
        InfoFragment infoPbj = new InfoFragment();
        prodObj.setArguments(bundle);
        infoPbj.setArguments(bundle);

        // set fragment
        myPagerAdapter.addFrag(prodObj,"Produk");
        myPagerAdapter.addFrag(infoPbj,"Info");
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

    }
}
