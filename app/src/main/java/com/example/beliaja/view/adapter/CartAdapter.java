package com.example.beliaja.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Cart.CartAdapterPresenter;
import com.example.beliaja.presenter.Cart.CartPresenter;
import com.example.beliaja.presenter.Cart.CartView;
import com.example.beliaja.view.activity.AddCartActivity;
import com.example.beliaja.view.activity.DetailProductActivity;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Keranjang;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.DialogBuilder;
import com.example.beliaja.utility.ImgDownloader;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> implements CartView.AdapterView{
    Context context;
    CartPresenter cp;
    SessionManager session;
    List<Keranjang> keranjang;
    DialogBuilder dialogBuilder;
    HashMap<String, String> profile;
    CartAdapterPresenter presenter;


    public CartAdapter(Context context, List<Keranjang> keranjang, CartPresenter cartPresenter) {
        this.context = context;
        this.keranjang = keranjang;
        this.session = new SessionManager(context);
        this.profile = session.getData();
        dialogBuilder = new DialogBuilder();
        cp = cartPresenter;
        presenter = new CartAdapterPresenter(this);
    }

    public class  ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cart_toko)      TextView txt_toko;
        @BindView(R.id.cart_title)     TextView txt_title;
        @BindView(R.id.cart_price)     TextView txt_price;
        @BindView(R.id.cart_notes)     TextView txt_notes;
        @BindView(R.id.cart_change)    TextView txt_edit;
        @BindView(R.id.cart_count)     EditText ed_count;
        @BindView(R.id.cart_cover)     ImageView img_cover;
        @BindView(R.id.cart_dell)      ImageView img_dell;
        @BindView(R.id.cart_inc)       CircleImageView img_inc;
        @BindView(R.id.cart_dec)       CircleImageView img_dec;
        @BindView(R.id.cart_wishlist)  CircleImageView img_wishlist;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_cart,viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        final Keranjang mKeranjang = keranjang.get(position);

        viewHolder.txt_toko.setText(mKeranjang.getToko_nama());
        viewHolder.txt_title.setText(mKeranjang.getProduk_nama());
        viewHolder.txt_price.setText(mKeranjang.getProduk_harga());
        viewHolder.txt_notes.setText(mKeranjang.getNotes());
        viewHolder.ed_count.setText(mKeranjang.getJumlah());

        final String url = Constant.PROD_URL + mKeranjang.getProduk_pict();
        ImgDownloader.downloadAndSetImg(url, viewHolder.img_cover);

        if(Integer.valueOf(mKeranjang.getJumlah()) > 1){
            viewHolder.img_dec.setBorderColor(-1086464);
            viewHolder.img_dec.setImageResource(R.mipmap.ic_decrease);
        }

        viewHolder.img_inc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.ed_count.getText().toString().equals("1")){
                    viewHolder.img_dec.setBorderColor(-1086464);
                    viewHolder.img_dec.setImageResource(R.mipmap.ic_decrease);
                }

                int cnt = Integer.valueOf(viewHolder.ed_count.getText().toString()) + 1;
                viewHolder.ed_count.setText(String.valueOf(cnt));
                presenter.onIncreaseEvent(mKeranjang.getProduk_harga().replace(".", ""));
            }
        });

        viewHolder.img_dec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int cnt = Integer.valueOf(viewHolder.ed_count.getText().toString()) - 1;

                if(cnt < 1){
                    cnt = 1;
                }
                else if (cnt == 1){
                    viewHolder.img_dec.setBorderColor(-7566196);
                    viewHolder.img_dec.setImageResource(R.mipmap.ic_dec_gray);
                    presenter.onDecreaseEvent(mKeranjang.getProduk_harga().replace(".", ""));
                }

                else {
                    presenter.onDecreaseEvent(mKeranjang.getProduk_harga().replace(".", ""));
                };
                viewHolder.ed_count.setText(String.valueOf(cnt));
            }
        });

        viewHolder.txt_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.textTitleEvent(mKeranjang.getToko_id(), mKeranjang.getProduk_id());
            }
        });

        viewHolder.txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.textUbahEvent(
                        mKeranjang.getProduk_id(), mKeranjang.getProduk_nama(),
                        mKeranjang.getProduk_harga(), url
                );
            }
        });

        viewHolder.img_dell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                presenter.sendRemoveCart(profile.get(session.USER_ID), mKeranjang.getProduk_id(), position, keranjang.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return keranjang.size();
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(context);
    }

    // untuk komunikasi antara adapter dengan fragment perhitungan harga total
    @Override
    public void increaseHandler(String harga) {
        cp.onIncreaseClickEvent(harga);
    }

    @Override
    public void decreaseHandler(String harga) {
        cp.onDecreaseClickEvent(harga);
    }

    @Override
    public void refreshAdapter(boolean status) {
        cp.onRefreshAdapterHandler(status);
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissPrg();
    }

    @Override
    public void removeSuccess(int position, int sizeOfObj) {
        dialogBuilder.dismissPrg();
        keranjang.remove(position);

        // posisi item yang dihapus
        notifyItemRemoved(position);
        // posisi akan berubah ketika ada list item yang dihapus
        notifyItemRangeChanged(position, sizeOfObj);
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(context, onFailure, Toast.LENGTH_SHORT).show();
        } else if( code == 501) {
            Toast.makeText(context, "Bad Request", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void textUbahHandler(String produk_id, String nama, String harga, String url_img) {
        Intent intent = new Intent(context, AddCartActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("prod_id", produk_id);
        bundle.putString("prod_title", nama);
        bundle.putString("prod_price", harga);
        bundle.putString("prod_img", url_img);

        intent.putExtras(bundle);

        context.startActivity(intent);
    }

    @Override
    public void textTitleHandler(String toko_id, String produk_id) {
        Intent intent = new Intent(context, DetailProductActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toko_id", toko_id);
        bundle.putString("produk_id", produk_id);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }
}
