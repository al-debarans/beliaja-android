package com.example.beliaja.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beliaja.R;
import com.example.beliaja.presenter.History.HistoryAdapterPresenter;
import com.example.beliaja.presenter.History.HistoryView;
import com.example.beliaja.view.activity.OrderListActivity;
import com.example.beliaja.model.Order;
import com.example.beliaja.utility.DateConverter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> implements HistoryView.AdapterView {

    Context context;
    List<Order> order;
    HistoryAdapterPresenter presenter;

    public HistoryAdapter(Context context, List<Order> order) {
        this.context = context;
        this.order = order;
        presenter = new HistoryAdapterPresenter(this);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.hist_id)     TextView txt_orderId;
        @BindView(R.id.hist_date)   TextView txt_date;
        @BindView(R.id.hist_status) TextView txt_status;
        @BindView(R.id.hist_card)   CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
    @NonNull
    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_history,viewGroup, false);

        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        String status = "";
        final Order mOrder = order.get(position);

        viewHolder.txt_orderId.setText(mOrder.getOrder_id());
        viewHolder.txt_date.setText(DateConverter.convert(mOrder.getCreated()));

        if(mOrder.getPay_status().equals("1")){
            status = "Sudah dibayar";
            viewHolder.txt_status.setText(status);
        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onCardViewClick(mOrder.getOrder_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return order.size();
    }

    @Override
    public void onCardViewHandler(String order_id) {
        Intent intent = new Intent(context, OrderListActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("order_id", order_id);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }
}

