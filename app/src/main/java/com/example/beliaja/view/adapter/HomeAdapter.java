package com.example.beliaja.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.beliaja.R;
import com.example.beliaja.model.Produk;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.ImgDownloader;

import java.util.List;

public class HomeAdapter extends ArrayAdapter<Produk> {
    public HomeAdapter(@NonNull Context context, @NonNull List<Produk> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Produk produk = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_home, parent, false);
        }

        TextView txt_title = (TextView) convertView.findViewById(R.id.home_adptr_title);
        TextView txt_price = (TextView) convertView.findViewById(R.id.home_adptr_price);
        ImageView img_cover = (ImageView) convertView.findViewById(R.id.home_adptr_img);

        txt_title.setText(produk.getProduk_nama());
        txt_price.setText(produk.getProduk_harga());

        String url = Constant.PROD_URL + produk.getProduk_pict();
        ImgDownloader.downloadAndSetImg(url, img_cover);

        return convertView;
    }
}
