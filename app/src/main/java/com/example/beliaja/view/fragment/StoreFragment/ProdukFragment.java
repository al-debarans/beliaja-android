package com.example.beliaja.view.fragment.StoreFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.model.Produk;
import com.example.beliaja.presenter.Store.FrgProdukPresenter;
import com.example.beliaja.presenter.Store.StoreView;
import com.example.beliaja.view.adapter.FragmentProdukAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProdukFragment extends Fragment implements StoreView.ViewProduk {

    Bundle bundle;
    FrgProdukPresenter presenter;
    FragmentProdukAdapter frgProdAdapter;

    public ProdukFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.strfrg_recycle)    RecyclerView recyclerView;
    @BindView(R.id.strfrg_progress)   ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_produk, container, false);
        ButterKnife.bind(this, view);
        presenter = new FrgProdukPresenter((this));

        bundle = this.getArguments();
        String[] data = bundle.getStringArray("data");
        presenter.fetchStoreProduk(data[0]);

        return view;
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getActivity(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 404) {
            Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void isEmpty(boolean status) {
        if(status){
            progressBar.setVisibility(View.GONE);
        } else {
            Log.e("status", "not empty");
            progressBar.setVisibility(View.GONE);
            recyclerView.setAdapter(frgProdAdapter);
        }
    }

    @Override
    public void initAdapterProduk(List<Produk> produkList) {
        frgProdAdapter = new FragmentProdukAdapter(getActivity(), produkList);
        RecyclerView.LayoutManager layoutManager = new
                GridLayoutManager(getActivity(), 2);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

}
