package com.example.beliaja.view.fragment.StoreFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.beliaja.R;
import com.example.beliaja.utility.DateConverter;
import com.example.beliaja.view.adapter.FragmentProdukAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class InfoFragment extends Fragment {

    Bundle bundle;

    public InfoFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.strfrg_desk)
    TextView txt_desk;
    @BindView(R.id.strfrg_lokasi)   TextView txt_loc;
    @BindView(R.id.strfrg_reputasi) TextView txt_rating;
    @BindView(R.id.strfrg_created) TextView txt_date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ButterKnife.bind(this, view);

        bundle = this.getArguments();
        String[] data = bundle.getStringArray("data");
        txt_desk.setText(data[3]);
        txt_loc.setText(data[4]);
        txt_rating.setText(data[5]);
        txt_date.setText(
                DateConverter.convert(data[6])
        );
        return view;
    }

}
