package com.example.beliaja.view.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.History.HistoryPresenter;
import com.example.beliaja.presenter.History.HistoryView;
import com.example.beliaja.view.adapter.HistoryAdapter;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Order;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends AppCompatActivity implements HistoryView.View {

    SessionManager session;
    HistoryPresenter presenter;
    HistoryAdapter historyAdapter;
    HashMap<String, String> profile;

    @BindView(R.id.hist_notfound)   TextView txt_empty;
    @BindView(R.id.hist_progress)   ProgressBar progressBar;
    @BindView(R.id.hist_recycle)    RecyclerView recyclerView;
    @BindView(R.id.hist_swipe)      SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setOnMain();

        getSupportActionBar().setTitle("History Order");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setAdapter(null);
                presenter.fetchHistory(profile.get(session.USER_ID));
            }
        });

        presenter.fetchHistory(profile.get(session.USER_ID));
    }

    private void setOnMain(){

        session         = new SessionManager(this);
        profile         = session.getData();

        ButterKnife.bind(this);
        presenter = new HistoryPresenter(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    @Override
    public void displayToastError(int code, String onFailure) {
        if( onFailure.length() != 0 ){
            Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
        } else if( code == 400 ){
            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Unknown error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayHistory(List<Order> historyList) {
        historyAdapter = new HistoryAdapter(HistoryActivity.this, historyList);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(HistoryActivity.this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void setAdapter() {
        recyclerView.setAdapter(historyAdapter);
    }

    @Override
    public void displayProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayEmptyMsg(boolean status) {
        if(status){
            txt_empty.setVisibility(View.VISIBLE);
        } else{
            txt_empty.setVisibility(View.GONE);
        }
    }

    @Override
    public void swipeRefreshEvent(boolean status) {
        swipeRefresh.setRefreshing(status);
    }
}
