package com.example.beliaja.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Home.HomeFragmentPresenter;
import com.example.beliaja.presenter.Home.HomeFragmentContract;
import com.example.beliaja.view.activity.DetailProductActivity;
import com.example.beliaja.view.adapter.HomeAdapter;
import com.example.beliaja.model.Produk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements HomeFragmentContract {

    private HomeFragmentPresenter presenter;
    private List<Produk> produkList;

    public HomeFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.frg_home_grid) GridView gridView;
    @BindView(R.id.frg_home_prgbar) ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initUI(view);

        return view;
    }

    private void initUI(View view){
        ButterKnife.bind(this, view);
        initPresenter();
        onAttachView();

        presenter.produk("");
        onEventListener();
    }

    private void onEventListener(){
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                presenter.onItemClickEvent(position);
            }
        });
    }

    @Override
    public void onSHowToast(String onFailure) {
        Toast.makeText(getActivity(), onFailure, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayProduk(final List<Produk> produkList) {
        this.produkList = produkList;
        gridView.setAdapter(new HomeAdapter(getActivity(), produkList));
    }

    @Override
    public void displayProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClickHandler(int position) {
        Intent intent = new Intent(getActivity(), DetailProductActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("toko_id", produkList.get(position).getToko_id());
        bundle.putString("produk_id", produkList.get(position).getProduk_id());
        intent.putExtras(bundle);

        startActivity(intent);
    }

    private void initPresenter(){
        presenter = new HomeFragmentPresenter();
    }

    @Override
    public void onAttachView() {
        presenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        presenter.onDettach();
    }

    @Override
    public void onDestroy() {
        onAttachView();
        super.onDestroy();
    }
}
