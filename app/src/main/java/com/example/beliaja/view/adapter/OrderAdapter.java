package com.example.beliaja.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Order.OrderAdapterPresenter;
import com.example.beliaja.presenter.Order.OrderPresenter;
import com.example.beliaja.presenter.Order.OrderView;
import com.example.beliaja.view.activity.OrderListActivity;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.Order;
import com.example.beliaja.utility.DateConverter;
import com.example.beliaja.utility.DialogBuilder;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderAdapter  extends RecyclerView.Adapter<OrderAdapter.ViewHolder>  implements OrderView.AdapterView {

    Context context;
    List<Order> order;
    SessionManager session;
    DialogBuilder  dialogBuilder;
    OrderPresenter odp;
    OrderAdapterPresenter presenter;

    HashMap<String, String> profile;

    public OrderAdapter(Context context, List<Order> order, OrderPresenter orderPresenter) {
        this.context = context;
        this.order = order;
        this.session = new SessionManager(context);
        this.profile = session.getData();
        this.odp = orderPresenter;
        presenter = new OrderAdapterPresenter(this);
        dialogBuilder = new DialogBuilder();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.order_id)     TextView txt_orderId;
        @BindView(R.id.order_date)   TextView txt_date;
        @BindView(R.id.order_status) TextView txt_status;
        @BindView(R.id.order_pay)    TextView btn_pay;
        @BindView(R.id.order_card)   CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_order,viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {

        String status = "";
        final Order mOrder = order.get(position);

        viewHolder.txt_orderId.setText(mOrder.getOrder_id());
        viewHolder.txt_date.setText(DateConverter.convert(mOrder.getCreated()));

        if(mOrder.getPay_status().equals("0")){
            status = "Belum dibayar";
            viewHolder.txt_status.setText(status);
        }

        viewHolder.btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendCheckoutClick(profile.get(session.USER_ID), mOrder.getOrder_id());
            }
        });

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onCardViewClick(mOrder.getOrder_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return order.size();
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(context);
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissPrg();
    }

    @Override
    public void refreshAdapter(boolean status) {
        odp.onRefreshAdapterEvent(true);
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(context, onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 400) {
            Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void cardViewHandler(String order_id) {
        Intent intent = new Intent(context, OrderListActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString("order_id", order_id);
        intent.putExtras(bundle);

        context.startActivity(intent);
    }
}