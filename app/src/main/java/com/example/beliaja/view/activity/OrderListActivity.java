package com.example.beliaja.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.OrderList.OrderListPresenter;
import com.example.beliaja.presenter.OrderList.OrderListView;
import com.example.beliaja.utility.DialogBuilder;
import com.example.beliaja.view.adapter.OrderListAdapter;
import com.example.beliaja.model.OrderList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderListActivity extends AppCompatActivity implements OrderListView.View {

    Bundle bundle;
    DialogBuilder dialogBuilder;
    OrderListPresenter presenter;
    OrderListAdapter orderListAdapter;

    @BindView(R.id.ordl_recycle)    RecyclerView recyclerView;
    @BindView(R.id.ordl_progress)   ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        setOnMain();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setOnMain(){

        dialogBuilder = new DialogBuilder();
        presenter = new OrderListPresenter(this);
        bundle = getIntent().getExtras();
        String title = bundle.getString("order_id");

        ButterKnife.bind(this);

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter.fetchOrderList(title);
    }


    @Override
    public void displayToastError(int code, String onFailure) {

        if(onFailure.length() != 0){
            Toast.makeText(getApplicationContext(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 400){
            Toast.makeText(getApplicationContext(), "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Unknown error", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void initAdapter(List<OrderList> orderList) {

        orderListAdapter = new OrderListAdapter(OrderListActivity.this, orderList);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(OrderListActivity.this);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void isEmpty(boolean status) {
        progressBar.setVisibility(View.GONE);
        if(!status){
            recyclerView.setAdapter(orderListAdapter);
        }
    }

}
