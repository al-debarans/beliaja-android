package com.example.beliaja.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Cart.CartPresenter;
import com.example.beliaja.presenter.Cart.CartView;
import com.example.beliaja.view.adapter.CartAdapter;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Keranjang;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.ApiInterface;
import com.example.beliaja.utility.DialogBuilder;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements CartView.View {
    int total;
    DecimalFormat df;
    SessionManager session;
    DialogBuilder dialogBuilder;
    HashMap<String, String> profile;
    CartAdapter cartAdapter;
    CartPresenter presenter;

    @BindView(R.id.cart_progress)   ProgressBar progressBar;
    @BindView(R.id.cart_recycle)    RecyclerView recyclerView;
    @BindView(R.id.cart_notfound)   TextView txt_empty;
    @BindView(R.id.cart_tot_price)  TextView txt_price_tot;
    @BindView(R.id.cart_main)       LinearLayout layout_main;
    @BindView(R.id.cart_bottom)     LinearLayout layout_bottom;
    @BindView(R.id.cart_swipe)      SwipeRefreshLayout swipeRefresh;

    @OnClick(R.id.cart_checkout) void confirm(){

        presenter.sendCheckout(profile.get(session.USER_ID));
    }

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view       = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, view);

        df = new DecimalFormat("#,###");

        session = new SessionManager(getActivity());
        profile = session.getData();
        presenter = new CartPresenter(this);

        dialogBuilder = new DialogBuilder();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                recyclerView.setAdapter(null);
                presenter.fetchKeranjang(profile.get(session.USER_ID));
            }
        });

        presenter.fetchKeranjang(profile.get(session.USER_ID));

        return  view;
    }

    @Override
    public void displayProgressBar() {
        dialogBuilder.showDialogPrg(getActivity());
    }

    @Override
    public void dismissProgressBar() {
        dialogBuilder.dismissPrg();
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
        txt_price_tot.setText(df.format(total).replace(",", "."));
    }

    @Override
    public void isEmpty(boolean status) {
        if(status){
            progressBar.setVisibility(View.GONE);
            layout_main.setVisibility(View.GONE);
            layout_bottom.setVisibility(View.GONE);
            txt_empty.setVisibility(View.VISIBLE);
        } else {
            txt_empty.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            layout_main.setVisibility(View.VISIBLE);
            layout_bottom.setVisibility(View.VISIBLE);
            recyclerView.setAdapter(cartAdapter);
        }
    }

    @Override
    public void initAdapter(List<Keranjang> keranjangList) {
        cartAdapter = new CartAdapter(getActivity(), keranjangList, presenter);
        RecyclerView.LayoutManager layoutManager = new
                LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void increaseHandler(String harga) {
        total = total + Integer.valueOf(harga);
        txt_price_tot.setText(df.format(total).replace(",", "."));
    }

    @Override
    public void decreaseHandler(String harga) {
        total = total - Integer.valueOf(harga);
        txt_price_tot.setText(df.format(total).replace(",", "."));
    }

    @Override
    public void refreshAdapter(boolean status) {
        if (status){
            recyclerView.setAdapter(null);
            presenter.fetchKeranjang(profile.get(session.USER_ID));
        }
    }

    @Override
    public void isSuccessCheckout(boolean status) {
        dialogBuilder.dismissPrg();
        presenter.fetchKeranjang(profile.get(session.USER_ID));
    }

    @Override
    public void swipeRefreshEvent(boolean status) {
        swipeRefresh.setRefreshing(status);
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getActivity(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 400){
            Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
        } else if (code == 501){
            Toast.makeText(getActivity(), "Bad Request", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }
}
