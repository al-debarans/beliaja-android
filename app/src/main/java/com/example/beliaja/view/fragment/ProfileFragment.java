package com.example.beliaja.view.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.beliaja.R;
import com.example.beliaja.presenter.Profile.ProfilePresenter;
import com.example.beliaja.presenter.Profile.ProfileView;
import com.example.beliaja.view.activity.LoginActivity;
import com.example.beliaja.data.SessionManager;
import com.example.beliaja.model.CallResponse;
import com.example.beliaja.model.Pembeli;
import com.example.beliaja.rest.ApiInterface;
import com.example.beliaja.rest.ApiClient;
import com.example.beliaja.rest.Constant;
import com.example.beliaja.utility.ImgDownloader;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements ProfileView.View {

    SessionManager session;
    HashMap<String, String> profile;
    ProfilePresenter presenter;

    @BindView(R.id.frg_card_prof)     CardView cardView;
    @BindView(R.id.frg_prof_name)     TextView txt_name;
    @BindView(R.id.frg_prof_email)    TextView txt_email;
    @BindView(R.id.frg_prof_phone)    TextView txt_phone;
    @BindView(R.id.frg_prof_img)      ImageView imageView;
    @BindView(R.id.frg_prof_linier)   LinearLayout layout;
    @BindView(R.id.frg_prof_progress) ProgressBar progressBar;

    @OnClick(R.id.frg_prof_btnLogout) void logout(){
        session.destroySession();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        setOnCreate();

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Profile edit", Toast.LENGTH_SHORT).show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void setOnCreate(){
        session     = new SessionManager(getActivity());
        profile     = session.getData();

        presenter = new ProfilePresenter(this);
        presenter.fetchProfile(profile.get(session.EMAIL));
    }

    @Override
    public void dismissProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayProfile(List<Pembeli> pembeliList) {
        layout.setVisibility(View.VISIBLE);
        for(int i = 0; i < pembeliList.size(); i++){

            txt_name.setText(pembeliList.get(i).getPembeli_nama());
            txt_email.setText(pembeliList.get(i).getPembeli_email());
            txt_phone.setText(pembeliList.get(i).getPembeli_hp());
            String url_img = Constant.PROF_URL + pembeliList.get(i).getPembeli_pict();
            ImgDownloader.downloadAndSetImg(url_img, imageView);
        }
    }

    @Override
    public void displayToastError(int code, String onFailure) {
        if(onFailure.length() != 0){
            Toast.makeText(getActivity(), onFailure, Toast.LENGTH_SHORT).show();
        } else if (code == 404) {
            Toast.makeText(getActivity(), "Not Found", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Unknown Error", Toast.LENGTH_SHORT).show();
        }
    }
}
