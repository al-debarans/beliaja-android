package com.example.beliaja.base;

public interface Contract {
    void onAttachView();
    void onDetachView();
}
