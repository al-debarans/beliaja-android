package com.example.beliaja.base;

public interface Presenter<T extends Contract> {
    void onAttach(T contract);
    void onDettach();
}
