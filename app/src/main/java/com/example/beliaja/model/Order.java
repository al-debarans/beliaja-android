package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("order_id") private String order_id;
    @SerializedName("pay_status") private String pay_status;
    @SerializedName("created") private String created;

    public String getOrder_id() {
        return order_id;
    }

    public String getPay_status() {
        return pay_status;
    }

    public String getCreated() {
        return created;
    }
}
