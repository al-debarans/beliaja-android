package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Keranjang {


    @SerializedName("cart_id") private String cart_id;
    @SerializedName("produk_id") private String produk_id;
    @SerializedName("jumlah") private String jumlah;
    @SerializedName("notes") private String notes;
    @SerializedName("status_prod") private String status_prod;
    @SerializedName("toko_id") private String toko_id;
    @SerializedName("toko_nama") private String toko_nama;
    @SerializedName("produk_nama") private String produk_nama;
    @SerializedName("produk_harga") private String produk_harga;
    @SerializedName("produk_pict") private String produk_pict;

    public String getCart_id() {
        return cart_id;
    }

    public String getProduk_id() {
        return produk_id;
    }

    public String getStatus_prod() {
        return status_prod;
    }

    public String getToko_id() {
        return toko_id;
    }

    public String getToko_nama() {
        return toko_nama;
    }

    public String getProduk_nama() {
        return produk_nama;
    }

    public String getProduk_harga() {
        return produk_harga;
    }

    public String getProduk_pict() {
        return produk_pict;
    }

    public String getJumlah() {
        return jumlah;
    }

    public String getNotes() {
        return notes;
    }
}
