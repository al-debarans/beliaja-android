package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Wishlist {

    @SerializedName("wishlist_id") private String wishlist_id;
    @SerializedName("produk_id") private String produk_id;
    @SerializedName("toko_id") private String toko_id;
    @SerializedName("toko_nama") private String toko_nama;
    @SerializedName("toko_lokasi") private String toko_lokasi;
    @SerializedName("produk_nama") private String produk_nama;
    @SerializedName("produk_harga") private String produk_harga;
    @SerializedName("produk_pict") private String produk_pict;

    public String getWishlist_id() {
        return wishlist_id;
    }

    public String getProduk_id() {
        return produk_id;
    }

    public String getToko_id() { return toko_id; }

    public String getToko_nama() {
        return toko_nama;
    }

    public String getToko_lokasi() {
        return toko_lokasi;
    }

    public String getProduk_nama() {
        return produk_nama;
    }

    public String getProduk_harga() {
        return produk_harga;
    }

    public String getProduk_pict() {
        return produk_pict;
    }
}
