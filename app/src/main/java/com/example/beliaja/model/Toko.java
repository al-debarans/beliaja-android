package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Toko {

    @SerializedName("toko_id") private String toko_id;
    @SerializedName("toko_nama") private String toko_nama;
    @SerializedName("toko_lokasi") private String toko_lokasi;
    @SerializedName("toko_deskripsi") private String toko_deskripsi;
    @SerializedName("toko_reputasi") private String toko_reputasi;
    @SerializedName("toko_pict") private String toko_pict;
    @SerializedName("toko_created") private String toko_created;

    public String getToko_id() {
        return toko_id;
    }

    public String getToko_nama() {
        return toko_nama;
    }

    public String getToko_lokasi() {
        return toko_lokasi;
    }

    public String getToko_deskripsi() {
        return toko_deskripsi;
    }

    public String getToko_reputasi() {
        return toko_reputasi;
    }

    public String getToko_created() {
        return toko_created;
    }

    public String getToko_pict() { return toko_pict; }
}
