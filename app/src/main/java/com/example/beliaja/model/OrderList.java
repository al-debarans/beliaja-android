package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class OrderList {
    /*
    "order_id": "ORD120190324172800",
            "jumlah": "1",
            "produk_nama": "JAKET COWOK PRIA POLOS SWEATER HOODIE ZIPPER RESLETING BIRU TUTON - sesuai di foto, L",
            "produk_pict": "259660_868f9f91-5136-4e51-8a7e-9398b0182bd6_2048_2048.jpeg",
            "created": "2019-03-24 17:28:00"
     */

    @SerializedName("order_id") private String order_id;
    @SerializedName("jumlah") private String jumlah;
    @SerializedName("produk_nama") private String produk_nama;
    @SerializedName("produk_pict") private String produk_pict;
    @SerializedName("created") private String created;

    public String getOrder_id() {
        return order_id;
    }

    public String getJumlah() {
        return jumlah;
    }

    public String getProduk_nama() {
        return produk_nama;
    }

    public String getProduk_pict() {
        return produk_pict;
    }

    public String getCreated() {
        return created;
    }
}
