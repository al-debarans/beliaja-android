package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CallResponse {

    public String getResponse() {
        return response;
    }

    public List<Pembeli> getProfile() {
        return profile;
    }

    public List<Produk> getProduk() { return produk; }

    public List<Toko> getToko() { return toko; }

    public List<Wishlist> getWishlist() { return wishlist; }

    public List<Keranjang> getKeranjang() { return keranjang; }

    public List<Order> getOrder() { return order; }

    public List<OrderList> getOrderlist() { return orderlist; }

    @SerializedName("response") private String response;

    @SerializedName("profile") private List<Pembeli> profile;

    @SerializedName("produk") private List<Produk> produk;

    @SerializedName("toko") private List<Toko> toko;

    @SerializedName("wishlist") private List<Wishlist> wishlist;

    @SerializedName("keranjang") private List<Keranjang> keranjang;

    @SerializedName("order") private List<Order> order;

    @SerializedName("orderlist") private List<OrderList> orderlist;

}
