package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Pembeli {

    @SerializedName("pembeli_id") private String pembeli_id;
    @SerializedName("pembeli_nama") private String pembeli_nama;
    @SerializedName("pembeli_email") private String pembeli_email;
    @SerializedName("pembeli_password") private String pembeli_password;
    @SerializedName("pembeli_hp") private String pembeli_hp;
    @SerializedName("pembeli_alamat") private String pembeli_alamat;
    @SerializedName("pembeli_pict") private String pembeli_pict;
    @SerializedName("pembeli_created") private String pembeli_created;

    public String getPembeli_id() {
        return pembeli_id;
    }

    public String getPembeli_nama() {
        return pembeli_nama;
    }

    public String getPembeli_email() {
        return pembeli_email;
    }

    public String getPembeli_password() {
        return pembeli_password;
    }

    public String getPembeli_hp() {
        return pembeli_hp;
    }

    public String getPembeli_alamat() {
        return pembeli_alamat;
    }

    public String getPembeli_pict() {
        return pembeli_pict;
    }

    public String getPembeli_created() {
        return pembeli_created;
    }
}
