package com.example.beliaja.model;

import com.google.gson.annotations.SerializedName;

public class Produk {

    @SerializedName("produk_id") private String produk_id;
    @SerializedName("toko_id") private String toko_id;
    @SerializedName("produk_nama") private String produk_nama;
    @SerializedName("produk_harga") private String produk_harga;
    @SerializedName("produk_stock") private String produk_stock;
    @SerializedName("produk_pict") private String produk_pict;
    @SerializedName("produk_deskripsi") private String produk_deskripsi;
    @SerializedName("produk_created") private String produk_created;

    public String getProduk_id() {
        return produk_id;
    }

    public String getToko_id() {
        return toko_id;
    }

    public String getProduk_nama() {
        return produk_nama;
    }

    public String getProduk_harga() {
        return produk_harga;
    }

    public String getProduk_stock() {
        return produk_stock;
    }

    public String getProduk_pict() {
        return produk_pict;
    }

    public String getProduk_deskripsi() {
        return produk_deskripsi;
    }

    public String getProduk_created() {
        return produk_created;
    }
}
