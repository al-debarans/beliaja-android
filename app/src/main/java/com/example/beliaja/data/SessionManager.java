package com.example.beliaja.data;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.beliaja.view.activity.MainActivity;

import java.util.HashMap;

public class SessionManager {

    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "VidStreamingPref";

    private static final String IS_EXIST = "IsExist";

    public static final String USER_ID = "user_id";

    public static final String EMAIL = "pembeli_email";

    public static final String PASSWORD = "pembeli_password";

    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createSession(String user_id,String user_email, String user_password){
        editor.putBoolean(IS_EXIST, true);
        editor.putString(USER_ID, user_id);
        editor.putString(EMAIL, user_email);
        editor.putString(PASSWORD, user_password);
        editor.commit();
    }

    public void destroySession(){

        editor.clear();
        editor.commit();
    }

    public void checkExist(){
        if(this.isExist()){

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }
    }

    public HashMap<String, String> getData(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(EMAIL, pref.getString(EMAIL, null));
        user.put(PASSWORD, pref.getString(PASSWORD, null));
        return user;
    }

    public boolean isExist(){
        return pref.getBoolean(IS_EXIST, false);
    }

}
